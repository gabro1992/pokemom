
$("#btnRefrescar").click(function(e) {
    var html = '';
    e.preventDefault();
    var dato = $("#name").val();
    var route = "http://127.0.0.1:8000/trainers";
    var token = $("#token").val();
    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        data:{name: dato},
        beforeSend: function(){
            $( "#msj" ).html( "<div class='alert alert-success' role='alert' id='msjProc'>Procesando..</div>" );
        },
        success: function(respuesta) {
            $( ".row" ).html("");
            //$("#btnRefrescar").remove();
            respuesta.mensaje.forEach(element => {
                html +="<div class='col col-sm-4 mt-3'><div class='card' style='width: 16rem;'><img src='/images/"+element.avatar+"' class='card-img-top rounded-circle mx-auto' alt='Imagen' style='height;100px; width:100px; background-color:#EFEFEF'><div class='card-body'><h5 class='card-title text-center'>"+element.name+"</h5><p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p><a href='/trainers/"+element.slug+"' class='btn btn-primary'>Ver más...</a></div></div></div>";
            });
            $( ".row" ).append(html);
        },
        error: function() {
            console.log("No se ha podido obtener la información");

        },complete: function() {
            $("#msjProc").fadeOut();
        }

    });
});
