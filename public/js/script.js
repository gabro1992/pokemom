

console.log('hola');
$("#btnEnviar").click(function(e){
    e.preventDefault();
    var dato = $("#name").val();
    var route = "http://127.0.0.1:8000/trainers";
    var token = $("#token").val();
    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:{name: dato},
        beforeSend: function(){
            $( "#msj" ).html( "<div role='alert' id='msjProc'>Procesando..</div>" );
        },
        success: function(respuesta) {
            console.log(respuesta.mensaje);
            if(respuesta.mensaje){
                $("#name").val('');
                $( "#msj" ).html( "<div class='alert alert-success' role='alert' id='msjTemp'>Registrado con exito</div>" );
                setTimeout(function(){$("#msjTemp").fadeOut(2000); }, 1000);
            }else{
                $( "#msj" ).html( "<div class='alert alert-danger' role='alert' id='msjTemp'>Error, intente de nuevo.</div>" );
                setTimeout(function(){$("#msjTemp").fadeOut(2000); }, 1000);
            }
        },
        error: function() {
            console.log("No se ha podido obtener la información");

        },complete: function() {
            $("#msjProc").fadeOut();
        }

    });
});
