<?php

use Illuminate\Database\Seeder;
use Pivca\Role;
use Pivca\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Esto es para datos de prueba, para evitar crear CRUD de usuarios, por ahora.
        $role_user = Role::where('name','user')->first();
        $role_admin = Role::where('name','admin')->first();

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('query');
        $user->save();
        //Para que se relacionen los modelos, attach recibe al role_user
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@gmail.com';
        $user->password = bcrypt('query');
        $user->save();
        //Para que se relacionen los modelos, attach recibe al rol role_admin
        $user->roles()->attach($role_admin);
    }
}
