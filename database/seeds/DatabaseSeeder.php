<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Primero se ejecuta el Role para evitar fallas.
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
