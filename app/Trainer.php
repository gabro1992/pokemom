<?php

namespace Pivca;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $fillable = ['name','avatar'];
}
