<?php

namespace Pivca\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Pivca\Trainer;
use Illuminate\Http\Request;
use Pivca\Role;
use Pivca\User;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //VERICAR que haya conexión a la BBDD, pendiente;
        $request->user()->authorizeRoles('admin');
        if ($request->ajax()) {
            $trainers = Trainer::get(['id','name', 'avatar','slug']);
            return response()->json([
                "mensaje" =>  $trainers
            ]);
        }
        return view('trainers.index');
        //$trainers = Trainer::all();//Todos los campos
       // return view('trainers.index', compact('trainers'));//Sin AJAX
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Prueba de validación nro mayor a cero.
        /*if ($request->input('avos')<=0) {
            return view('trainers.index');
        }*/

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'./././images/', $name);
        }

        $trainer = new Trainer();
        $trainer->name= $request->input('name');
        $trainer->avatar = $name;
        $trainer->slug= $request->input('slug');
        $trainer->avos = 1;
        if($trainer->save()){
            //return view('trainers.index');
            return redirect()->route('trainers.index')->with('status','Entrenador Registrado');
        }
        /*if ($request->ajax()) {
            try {
                $trainer = new Trainer();
                $trainer->name= $request->input('name');
                if($trainer->save()){
                    return response()->json([
                        "mensaje" =>  'ok'
                    ]);
                }else{
                    return response()->json([
                        "mensaje" =>  'error'
                    ]);
                }
            } catch (\Throwable $th) {
                return response()->json([
                    "mensaje" =>  false
                ]);
            }

        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $trainer = Trainer::where('slug', $slug)->get(['id','name','avatar','slug']);
        //$trainer = Trainer::where('slug', $slug)->paginate(1);
        return view('trainers.show', compact('trainer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $trainer = Trainer::where('slug', $slug)->get(['id','name','avatar','slug']);
        return view('trainers.edit', compact('trainer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $trainer = Trainer::where('slug', $slug)->get(['id','name','avatar','slug']);

        $trainer[0]->fill($request->except('avatar'));
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $trainer[0]->avatar = $name;
            $file->move(public_path().'./././images/', $name);
        }

        $trainer[0]->save();
        return view('trainers.show', compact('trainer'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $trainer = Trainer::where('slug', $slug)->get(['id','avatar']);
        $file_path = public_path().'/images/'.$trainer[0]->avatar;
        //Elimina la imagen de la carpeta images del proyecto
        \File::delete($file_path);
        //Elimina el registro de la BBDD.
        $trainer[0]->delete();
        return view('trainers.index');
        return 'Entrenador Eliminado';
    }
}
