<?php

namespace Pivca\Http\Controllers;

use Pivca\Http\Controllers\Controller;

class PruebaController extends Controller {
    public function prueba($name = null){
        return 'Estoy dentro de prueba Controller: '.$name;
    }
}
