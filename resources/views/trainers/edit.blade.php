@extends('layouts.app')

@section('title','Trainer Edit')

@section('content')
    <br><br><br>
    <form class="form-group" action="/trainers/{{$trainer[0]->slug}}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <!--input type="hidden" name="_token" id="token" value="{{ csrf_token() }}"-->
        <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" class="form-control" name="name" id="name" value='{{$trainer[0]->name}}'>
        </div>

        <div class="form-group">
            <label for="">Avatar</label>
            <input type="file" name="avatar" id="file">
        </div>

        <button type="submit" class="btn btn-primary" id="btnEnviar">Actualizar</button>
    </form>
    <div class="container" id="msj"></div>

    <!--script src="/js/script.js"></script-->
@endsection
