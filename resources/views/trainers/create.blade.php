@extends('layouts.app')

@section('title','Trainers Create')

@section('content')
    <br><br><br>
    <form class="form-group" action="/trainers" method="POST" enctype="multipart/form-data">
        @csrf
        <!--input type="hidden" name="_token" id="token" value="{{ csrf_token() }}"-->
        <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" class="form-control" name="name" id="name">
        </div>

        <div class="form-group">
            <label for="">Slug</label>
            <input type="text" class="form-control" name="slug" id="slug">
        </div>

        <div class="form-group">
            <label for="">Avatar</label>
            <input type="file" name="avatar" id="file">
        </div>

        <button type="submit" class="btn btn-primary" id="btnEnviar">Guardar</button>
    </form>
    <div class="container" id="msj"></div>

    <!--script src="/js/script.js"></script-->
@endsection
