@extends('layouts.app')

@section('title','Trainer')

@section('content')
    <br><br><br>
    <h1>Testing</h1>
    <img src='/images/{{$trainer[0]->avatar}}' class='card-img-top rounded-circle mx-auto d-block' alt='Imagen' style='height:200px; width:200px; margin:20px; background-color:#EFEFEF'>
    <div class='text-center'>
        <h5 class='card-title'>{{$trainer[0]->name}}</h5>
        <p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.</p>

        <a href='/trainers/{{$trainer[0]->slug}}/edit' class='btn btn-primary mb-1'>Editar</a>

        <form class="form-group" action="/trainers/{{$trainer[0]->slug}}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-primary btn-danger" id="btnELiminar">Eliminar</button>
        </form>
    </div>
@endsection

