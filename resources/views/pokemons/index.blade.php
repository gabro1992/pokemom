@extends('layouts.app')
    @section('content')
        <div id="app">
            <br>
            <add-pokemon-btn></add-pokemon-btn>
            <pokemons-component></pokemons-component>
            <create-form-pokemon></create-form-pokemon>
        </div>

    @endsection
